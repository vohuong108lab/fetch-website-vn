import React, { useState } from "react";
import { makeStyles } from "@mui/styles";
import logo from "../assets/images/logo.svg";
import { Button, Grid, TextField, useMediaQuery } from "@mui/material";
import BusinessIcon from "@mui/icons-material/Business";
import ContactMailIcon from "@mui/icons-material/ContactMail";
import CallIcon from "@mui/icons-material/Call";
import FacebookIcon from "@mui/icons-material/Facebook";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import TwitterIcon from "@mui/icons-material/Twitter";
import CopyrightIcon from "@mui/icons-material/Copyright";
import { useTheme } from "@mui/material";
import { Link } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  footer: {
    width: "100%",
  },
  logo: {
    height: "3rem",
    [theme.breakpoints.down("md")]: {
      height: "2rem",
    },
  },
  footerContainer: {
    padding: "50px 10%",
    backgroundColor: "#000000",
    [theme.breakpoints.down('lg')]:{
      padding: "50px 7%"
    },
    [theme.breakpoints.down('sm')]:{
      padding: "24px 16px"
    }
  },
  tab: {
    ...theme.typography.tab,
    color: theme.palette.common.grey,
    fontWeight: "500",
    textDecoration: "none",
  },

  tabNav: {
    "&:hover": {
      color: theme.palette.secondary.main,
    },
  },
  tabHeader: {
    ...theme.typography.tab,
    color: theme.palette.common.yellow,
    fontWeight: "500",
  },
  footerMargin: {
    marginTop: 16,
  },
  button: {
    textTransform: "none",
    backgroundColor: theme.palette.common.yellow,
  },
}));

function Footer() {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down("lg"));
  const [email, setEmail] = useState("");
  const onEmailChange = (e) => {
    setEmail(e.target.value)
  }
  console.log(matches);
  return (
    <footer style={{textAlign: "start"}}>
      <div className={classes.footerMargin}></div>
      <Grid
        className={classes.footerContainer}
        container
        direction="row"
        columnSpacing={{ md: 3, sm: 6 }}
        rowSpacing={2}
      >
        <Grid
          rowSpacing={1}
          container
          direction="column"
          sm={6}
          item
          lg={3}
          md={4}
        >
          <Grid item>
            <img src={logo} className={classes.logo} />
          </Grid>

          <Grid container item>
            <Grid item style={{ width: "100%" }}>
              <TextField
                fullWidth
                id="outlined-basic"
                label="Enter your email"
                variant="outlined"
                style={{
                  borderRadius: 4,
                }}
                type="email"
                margin="normal"
                color="secondary"
                value={email}
                onChange = {onEmailChange}
                sx={{
                  "& label": {
                    color: theme.palette.common.grey,
                  },
                  "& label.Mui-focused": {
                    color: theme.palette.primary.main,
                  },

                  "& .MuiOutlinedInput-root": {
                    "& fieldset": {
                      borderColor: theme.palette.common.grey,
                    },
                    "&:hover fieldset": {
                      borderColor: theme.palette.primary.main,
                    },
                    "&.Mui-focused fieldset": {
                      borderColor: theme.palette.primary.main,
                      color: "white",
                    },
                    "& input": {
                      color: "white!important",
                    },
                  },
                }}
              />
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                style={{ backgroundColor: theme.palette.secondary.main }}
              >
                <span
                  style={{
                    color: "white",
                    fontWeight: 600,
                    textTransform: "none",
                  }}
                >
                  Sign in
                </span>
              </Button>
            </Grid>
          </Grid>
          <Grid
            container
            wrap="nowrap"
            direction="row"
            alignItems="flex-start"
            className={classes.tab}
            item
            style={{ color: "#9e9e9e", fontSize: "14px" }}
          >
            <Grid item style={{ paddingRight: 8 }}>
              <CopyrightIcon
                style={{ color: "#9e9e9e", fontSize: "14px" }}
              ></CopyrightIcon>
            </Grid>

            <Grid>2020 Fetch Technology Pte.Ltd.All Rights Reserved</Grid>
          </Grid>
        </Grid>
        <Grid
          rowSpacing={1}
          container
          item
          lg={3}
          md={4}
          sm={6}
          direction="column"
          justifyContent="flex-start"
        >
          <Grid className={classes.tabHeader} item>
            Company
          </Grid>
          <Grid rowSpacing={1} container item direction="column">
            <Grid
              component={Link}
              to="/about"
              className={[classes.tab, classes.tabNav]}
              item
            >
              About us
            </Grid>
            <Grid
              component={Link}
              to="/life"
              className={[classes.tab, classes.tabNav]}
              item
            >
              Life at fetch
            </Grid>
            <Grid
              component={Link}
              to="/jobs"
              className={[classes.tab, classes.tabNav]}
              item
            >
              Available jobs
            </Grid>
            <Grid
              component={Link}
              to="/candidates"
              className={[classes.tab, classes.tabNav]}
              item
            >
              What we look for
            </Grid>
          </Grid>
        </Grid>
        <Grid
          rowSpacing={2}
          container
          item
          lg={3}
          md={4}
          direction="column"
          justifyContent="flex-start"
        >
          <Grid className={classes.tabHeader} item>
            Contact Info
          </Grid>
          <Grid rowSpacing={1} container item direction="column">
            <Grid
              container
              wrap="nowrap"
              direction="row"
              alignItems="flex-start"
              className={classes.tab}
              item
            >
              <Grid item style={{ paddingRight: 8 }}>
                <BusinessIcon></BusinessIcon>
              </Grid>
              <Grid>
                Lane 82, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District
              </Grid>
            </Grid>
            <Grid
              container
              wrap="nowrap"
              direction="row"
              alignItems="flex-start"
              className={classes.tab}
              item
            >
              <Grid item style={{ paddingRight: 8 }}>
                <ContactMailIcon></ContactMailIcon>
              </Grid>
              <Grid>recruitment@fetch.tech</Grid>
            </Grid>
            <Grid
              container
              wrap="nowrap"
              direction="row"
              alignItems="flex-start"
              className={classes.tab}
              item
            >
              <Grid item style={{ paddingRight: 8 }}>
                <CallIcon></CallIcon>
              </Grid>
              <Grid>+84 843382474</Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          container
          item
          rowSpacing={2}
          lg={3}
          md={8}
          direction="column"
          justifyContent="flex-start"
        >
          <Grid className={classes.tabHeader} item>
            Our social media
          </Grid>
          <Grid
            rowSpacing={1}
            wrap="nowrap"
            container
            item
            direction={matches ? "row" : "column"}
          >
            <Grid
              container
              wrap="nowrap"
              direction="row"
              alignItems="flex-start"
              className={classes.tab}
              item
            >
              <Grid item style={{ paddingRight: 8 }}>
                <FacebookIcon></FacebookIcon>
              </Grid>
              <Grid>Facebook</Grid>
            </Grid>
            <Grid
              container
              wrap="nowrap"
              direction="row"
              alignItems="flex-start"
              className={classes.tab}
              item
            >
              <Grid item style={{ paddingRight: 8 }}>
                <LinkedInIcon></LinkedInIcon>
              </Grid>
              <Grid>LinkedIn</Grid>
            </Grid>
            <Grid
              container
              wrap="nowrap"
              direction="row"
              alignItems="flex-start"
              className={classes.tab}
              item
            >
              <Grid item style={{ paddingRight: 8 }}>
                <TwitterIcon></TwitterIcon>
              </Grid>
              <Grid>Twitter</Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </footer>
  );
}

export default Footer;
