import React from 'react';
import '../../assets/scss/Homepage.scss';
import bannerImg from '../../assets/img/6098c671ca8e3fd0669f.jpg';
import ourMissionImg from '../../assets/img/1a321e95196aec34b57b.jpg';
import wpfLeftImg from '../../assets/img/82c9c58bcd74382a6165.jpg';
import wpfRightImg from '../../assets/img/2e47f3d4f82b0d75543a.jpg';
import { Row, Col, Descriptions, Carousel, Collapse } from 'antd';
import { Link } from 'react-router-dom';
import ibmLogo from '../../assets/img/ibm.png';
import visaLogo from '../../assets/img/visa.png';
import dellLogo from '../../assets/img/dell.png';
import apcLogo from '../../assets/img/apc.png';
import mapImg from '../../assets/img/map.JPG';
import handshakeIcon from '../../assets/img/handshakeIcon.png';
import moneyHandIcon from '../../assets/img/moneyHandIcon.png';
import teamIcon from '../../assets/img/teamIcon.png';
import companyIcon from '../../assets/img/companyIcon.png';
import { ReactComponent as MapMarker} from '../../assets/img/map-marker.svg'; 
import { ReactComponent as Envelope} from '../../assets/img/envelope-regular.svg'; 
import { ReactComponent as Phone} from '../../assets/img/phone-alt-solid.svg'; 

const workPlaceData = [
    { icon: handshakeIcon, content: "hire the software developers, testers, designers in Vietnam" },
    { icon: moneyHandIcon, content: "more than $3,000,000 in salaries paid out till date" },
    { icon: teamIcon, content: "all kinds from seniorto junior engineers, skilledprogramers in various technologies for Singapore companies" },
    { icon: companyIcon, content: "more than 200 happy companies till date" }
]

const jobData = [
    {title: "Job", location: "Location"},
    {title: "Senior Engineering Manager", salary: "1000 - 2000 USD", location: "Ha Noi"},
    {title: "Senior Cocos2D Game Developer", salary: "1000 - 2000 USD", location: "Ha Noi"},
    {title: "DevOps Engineering (Senior, Middle)", salary: "1500 - 2200 USD", location: "Ha Noi"},
    {title: "Senior DevOps", salary: "1000 - 2000 USD", location: "Ho Chi Minh"},
    {title: "Ruby on Rails Developer", salary: "1500 - 3000 USD", location: "Ho Chi Minh"}
]

const Homepage = () => {
    return (
        <div className="homepage">
            <section className="homepage-banner">
                <div className="cus-container">
                    <div className="content-wrap">
                        <div className="company-name-wrap">
                            <h1>
                                FETCH
                                <div className="overlap-line" />
                            </h1>
                            <h1>
                                TECHNOLOGY
                                <div className="overlap-line" />
                            </h1>
                            <h1>
                                Vietnam
                                <div className="overlap-line" />
                            </h1>
                        </div>
                        <div className="banner-img-wrap">
                            <img src={bannerImg} alt="img-banner" />
                        </div>
                        <div className="company-slogan">
                            <h3>
                                hiring top talents
                                <div className="overlap-line" />
                            </h3>
                            <div className="v-line"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section className="workplace">
                <div className="cus-container">
                    <Row gutter={[{xs: 21, sm: 24, xl:30}, 0]} className="first-row">
                        <Col xs={20} sm={12} lg={6} className="first-row-col">
                            <CardWrap data={workPlaceData[0]} color="#2fcdbf"/>
                            <CardWrap data={workPlaceData[1]} color="#b09dcb"/>
                        </Col>
                        <Col xs={20} sm={12} lg={6} className="first-row-col">
                            <CardWrap data={workPlaceData[2]} color="#ccd5cb"/>
                            <CardWrap data={workPlaceData[3]} color="#e1565e"/>
                        </Col>
                        <Col xs={24} lg={12} className="first-row-col">
                            <div className="text-col-wrap">
                                <label>About</label>
                                <h2>need a great workplace?</h2>
                                <p>Fetch Technology Vietnam is a comprehensive global provider of HR 
                                    and Talent Acquisition Services, focusing primarily in the technology fields.
                                    Founded in 2016, Fetch Technology Vietnam helps foreign companies of all types
                                    and sizes reach their potential by providing the talent and support to efficiently
                                    build and scale a high-performing, distributed workforce in Vietnam.
                                </p>
                                <div role="button" className="btn-readmore">READ MORE</div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section className="mission">
                <div className="cus-container">
                    <Row gutter={{xs: 21, sm: 24, xl:30}} className="second-row">
                        <Col xs={24} lg={12} className="second-row-col">
                            <div className="text-left-wrap">
                                <h2>Our mission</h2>
                                <div>
                                    <p>Our mission is to offer Vietnam's most talented 
                                        technologists a platform to connect with some of 
                                        the world's leading tech companies and build their 
                                        expertise on a global scale
                                    </p>
                                    <Link className="link-learn-more">LEARN MORE</Link>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={12} className="second-row-col">
                            <div className="img-right-wrap">
                                <img src={ourMissionImg} alt="our mission" />
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section className="wp-fluid">
                <div className="cus-container-fluid">
                    <Row justify="space-between">
                        <Col xs={24} md={12} lg={14}>
                            <div className="left-wrap">
                                <img src={wpfLeftImg} alt="wpfLeftImg"/>
                                <p>
                                    Over 4 years, Fetch has build a good reputation and is trusted by many
                                    Vietnamese and foreign companies.
                                </p>
                            </div>
                        </Col>
                        <Col xs={24} md={12} lg={9}>
                            <div className="right-wrap">
                                <img src={wpfRightImg} alt="wpfRightImg" />
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section className="top-job">
                <div className="cus-container">
                    <Row justify="center" gutter={[{xs: 21, sm: 24, xl:30}, 0]}>
                        <Col span={24}>
                            <div className="content-wrap">
                                <h1>Top jobs</h1>
                                <Collapse className="collapse-job" bordered={false} ghost>
                                    {jobData.map((item, index) => {
                                        if (index === 0) return;
                                        else return (
                                            <Collapse.Panel header={item?.title} key={index}>
                                                {`${item?.location}: ${item?.salary}`}
                                            </Collapse.Panel>
                                        )
                                    })}
                                </Collapse>
                                <Descriptions colon={false} column={1} className="description-job">
                                    {jobData.map((item, index) => (
                                        <Descriptions.Item 
                                            key={index} 
                                            label={`${item?.title} ${item.salary ? `(${item?.salary})` : ""}`}
                                        >
                                            {item?.location}
                                        </Descriptions.Item>
                                    ))}
                                </Descriptions>
                                <div role="button" className="btn-see-all">SEE ALL JOBS</div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section className="trusted">
                <div className="cus-container">
                    <h2>Trusted by</h2>
                    <SlickTrusted />
                </div>
            </section>
            <section className="contact">
                <div className="contact-upper">
                    <div className="cus-container">
                        <Row gutter={[{xs: 21, sm: 24, xl:30}, 0]} justify="center">
                            <Col xl={15}>
                                <h2>Contact Us</h2>
                                <p>We align leaders around a shared purpose and strategic story that catalyzes their
                                    bussiness and brand to take action
                                </p>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div className="contact-lower">
                    <img src={mapImg} alt="map-img" />
                </div>
                <div className="contact-opt">
                    <div className="cus-container">
                        <Row className="contact-opt-row" gutter={[{xs: 21, sm: 24, xl:30}, 0]} >
                            <Col className="contact-opt-col" xs={24} sm={20} md={8} xl={8}>
                                <div className="content-wrap">
                                    <div className="icon-wrap">
                                        <MapMarker class="contact-icon map-marker"/>
                                    </div>
                                    <h4>Address:</h4>
                                    <p>
                                        Ho Chi Minh: BCONS 2 Level 3, 42/1 Ung Van Khiem Street, Ward 25, Binh Thanh District 
                                    </p>
                                    <p>
                                        Ha Noi: Lane 82, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District
                                    </p>
                                </div>
                            </Col>
                            <Col className="contact-opt-col" xs={24} sm={12} md={8} xl={8}>
                                <div className="content-wrap">
                                    <div className="icon-wrap">
                                        <Envelope class="contact-icon envelope"/>
                                    </div>
                                    <h4>Email:</h4>
                                    <p className="email">Recruitment@fetch.tech</p>
                                </div>
                            </Col>
                            <Col className="contact-opt-col" xs={24} sm={12} md={8} xl={8}>
                                <div className="content-wrap">
                                    <div className="icon-wrap">
                                        <Phone class="contact-icon phone"/>
                                    </div>
                                    <h4>Call us:</h4>
                                    <p className="phone-number">+84 843382474</p>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </section>
        
        </div>
    )
}

export default Homepage;

const CardWrap = ({ data, color }) => {
    return (
        <div className="card-wrap">
            <div className="icon-wrap" style={{backgroundColor: color}}>
                <img src={data.icon} alt="card-icon" />
            </div>
            <p>{data.content}</p>
        </div>
    )
}

const SlickTrusted = () => {
    const settings = {
        className: "slick-trusted",
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        cssEase: "linear",
        responsive: [
            {
                breakpoint: 5000,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    };
    return (
        <Carousel {...settings}>
            <div>
                <img src={apcLogo} alt=""/>
            </div>
            <div>
                <img src={dellLogo} alt=""/>
            </div>
            <div>
                <img src={visaLogo} alt=""/>
            </div>
            <div>
                <img src={ibmLogo} alt=""/>
            </div>
            <div>
                <img src={dellLogo} alt=""/>
            </div>
        </Carousel>
    )
}

