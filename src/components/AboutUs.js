import { Button, Grid, useMediaQuery } from "@mui/material";
import React from "react";
import { useTheme, makeStyles } from "@mui/styles";
import "../assets/scss/aboutus.scss";
import CheckIcon from "@mui/icons-material/Check";
import Icon from "@mui/material/Icon";
import aboutBg1 from "../assets/images/aboutBg1.jpg";
import aboutBg2 from "../assets/images/aboutBg2.jpg";
import aboutFeatureBg1 from "../assets/images/aboutFeatureBg1.png";
import aboutFeatureBg2 from "../assets/images/aboutFeatureBg2.png";
import aboutFeatureBg3 from "../assets/images/aboutFeatureBg3.png";
import aboutFeatureBg4 from "../assets/images/aboutFeatureBg4.png";
import aboutFeatureBg5 from "../assets/images/aboutFeatureBg5.png";
import aboutFeatureBg6 from "../assets/images/aboutFeatureBg6.png";
import FoodBankIcon from "@mui/icons-material/FoodBank";
import CardTravelIcon from "@mui/icons-material/CardTravel";
import CardGiftcardIcon from "@mui/icons-material/CardGiftcard";
import headerBg from "../assets/images/aboutHeaderBg.jpg"
const useStyles = makeStyles((theme) => ({
  headerBackground: {
    minHeight: "30em",
    "& img":{
      height: "30em"
    },
    [theme.breakpoints.down("md")]:{
     width: "100%",
     height: "auto",
     "& img":{
      width: "100%",
      height: "auto"
    },
    },
    [theme.breakpoints.up("lg")]:{
      "& img":{
       width: "100%",
       height: "auto",
       minHeight: "30em"
     },
     },
     [theme.breakpoints.down("sm")]:{
      minHeight: "0px"
     },
     overflow:"hidden"
  },
  feature: {
    backgroundPosition: "center",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    marginBottom:"10em!important",
    [theme.breakpoints.down("sm")]:{
      height:"10em",
      marginBottom:"5em!important",
    }
  },
  eventBackground: {
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    height: "40em",
  },
  eventBackground2: {
    minHeight: "10em",
    backgroundColor: theme.palette.secondary.main,
  },

  boxContainer: {
    padding: "20px 10%",
    [theme.breakpoints.down("md")]:{
      padding: "20px 16px"
    }
  },
  culture: {
    backgroundColor: theme.palette.secondary.main,
    padding: 10,
    borderRadius: 10,
    color: "white",
    maxWidth: "50%",
    minWidth: "200px",
    marginBottom: 40,
    display: "flex",
    justifyContent: "center",
  },
  event: {
    backgroundColor: theme.palette.grey.light,
    padding: 10,
    borderRadius: 10,
    marginBottom: "10px!important",
    boxShadow: "0px 10px 15px rgba(0,0,0,0.2)",
  },
  iconContainer: {
    borderRadius: "50%",
    height: 36,
    width: 36,
    backgroundColor: theme.palette.secondary.main,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  aboutBox: {
    backgroundColor: theme.palette.grey.light,
    padding: 20,
    borderRadius: 10,
    boxShadow: "0px 10px 15px rgba(0,0,0,0.2)",
    [theme.breakpoints.up("md")]: {
      transform: "transLateY(-80px)",
    },
  },
  avatarContainer:{
    height: 250,
    width: 250,
    borderRadius: "50%",
    backgroundColor: theme.palette.secondary.main,
    marginBottom: 30,
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  },
  workingEnv:{

  }
}));
function AboutUs() {
  const theme = useTheme();
  const classes = useStyles();
  const matchesMd = useMediaQuery(theme.breakpoints.down("md"))
  const matchesSM = useMediaQuery(theme.breakpoints.down("sm"))
  return (
    <section className="about-us">
      <Grid container className="about__inner">
        <Grid item container direction="row" className="about__header ">
          <Grid
            item
            container
            md={6}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{
              backgroundColor: theme.palette.secondary.main,
            }}
            className="c-cus-container"
          >
            <Grid item>
              <div className="c-h2 c--bold">FETCH TECHNOLOGY</div>
              <div style={{marginTop: 15}} className="c-h5">Hire developers with Fetch</div>
            </Grid>
          </Grid>
          <Grid item md={6} className={classes.headerBackground}>
            <img src={headerBg} alt="header bg"/>
          </Grid>
        </Grid>
        <Grid
          item
          container
          direction="column"
          className=""
          style={{ padding: !matchesSM?"50px 5% 140px":"50px 5% 50px" }}
        >
          <Grid
            item
            container
            justifyContent="center"
            style={{ marginBottom: 80 }}
          >
            <div className="c-h3 text-center c--bold">WHY PEOPLE CHOOSE US</div>
          </Grid>
          <Grid
            container
            item
            direction="row"
            rowSpacing={5}
            columnSpacing={{ md: 3, sm: 1 }}
          >
            <Grid
              container
              item
              alignItems="center"
              direction="column"
              lg={4}
              sm={6}
              className={classes.feature}
              style={{backgroundImage: `url(${aboutFeatureBg1})`}}
            >
              <div className="c-h5 c--bold grey-2">ATTRACTIVE SALARY</div>
              <div className="c-p feature-text text-center grey-3">
                competitive and attractive salary
              </div>
            </Grid>
            <Grid
              container
              item
              alignItems="center"
              lg={4}
              sm={6}
              className={classes.feature}
              style={{backgroundImage: `url(${aboutFeatureBg2})`}}
              direction="column"
            >
              <div className="c-h5 c--bold">INSURANCE</div>
              <div className="c-p feature-text text-center">
                Full social insurance, health insurance & unemployment insurance
              </div>
            </Grid>
            <Grid
              container
              item
              alignItems="center"
              lg={4}
              sm={6}
              className={classes.feature}
              style={{backgroundImage: `url(${aboutFeatureBg3})`}}
              direction="column"
            >
              <div className="c-h5 c--bold">GROWTH</div>
              <div className="c-p feature-text text-center">
                Professional & Personal Growth with extensive on job training
              </div>
            </Grid>
            <Grid
              container
              item
              alignItems="center"
              lg={4}
              sm={6}
              className={classes.feature}
              style={{backgroundImage: `url(${aboutFeatureBg4})`}}
              direction="column"
            >
              <div className="c-h5 c--bold">INTERNAL ACTIVITIES</div>
              <div className="c-p feature-text text-center">
                Play games, Play sports
              </div>
            </Grid>
            <Grid
              container
              item
              alignItems="center"
              lg={4}
              sm={6}
              className={classes.feature}
              style={{backgroundImage: `url(${aboutFeatureBg5})`}}
              direction="column"
            >
              <div className="c-h5 c--bold">WORKING ENVIRONMENT</div>
              <div className="c-p feature-text text-center">
                Friendly working environment both with foreigners and Vietnamese
              </div>
            </Grid>
            <Grid
              container
              item
              alignItems="center"
              lg={4}
              sm={6}
              className={classes.feature}
              style={{backgroundImage: `url(${aboutFeatureBg6})`}}
              direction="column"
            >
              <div className="c-h5 c--bold">ANNUAL LEAVE</div>
              <div className="c-p feature-text text-center">
                Special annual leave
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          container
          className=""
          style={{ backgroundColor: theme.palette.grey.light }}
        >
          <Grid item container className={["c-cus-container", classes.workingEnv]} justifyContent={matchesMd&&"center"}  md={6}>
            <Grid
              item
              container
              direction="column"
              style={{ marginBottom: 50 }}
            >
              <div
                className="c-h3"
                style={{
                  fontFamily: "Gilroy-Bold",
                  color: theme.palette.secondary.main,
                }}
              >
                LIFE IS GOOD
              </div>
              <div className="c-h5 c--bold" style={{ fontFamily: "Gilroy-Regular" }}>
                WORKING ENVIRONMENT
              </div>
            </Grid>
            <Grid
              item
              container
              direction="column"
              style={{ marginBottom: 100 }}
            >
              <Grid item container>
                <Icon component={CheckIcon} color="info" style={{marginRight: 10}}></Icon>
                <div className="c-p">Creative environment</div>
              </Grid>
              <Grid item container>
                <Icon component={CheckIcon} color="info" style={{marginRight: 10}}></Icon>
                <div className="c-p">Growth opportunities</div>
              </Grid>
              <Grid item container>
                <Icon component={CheckIcon} color="info" style={{marginRight: 10}}></Icon>
                <div className="c-p">Empowerment</div>
              </Grid>
              <Grid item container>
                <Icon component={CheckIcon} color="info" style={{marginRight: 10}}></Icon>
                <div className="c-p">Flexibility</div>
              </Grid>
              <Grid item container>
                <Icon component={CheckIcon} color="info" style={{marginRight: 10}}></Icon>
                <div className="c-p">Good work-life balance</div>
              </Grid>
            </Grid>
            <Grid item>
              <Button variant="contained" color="yellow" xs = {
                {
                  backgroundColor: theme.palette.secondary.main
                }
              }>
                Read more
              </Button>
            </Grid>
          </Grid>
          <Grid
            item
            container
            md={6}
            justifyContent="center"
            style={{ marginBottom: "5em", overflow: "hidden" }}
          >
            <img
              src={aboutBg1}
              alt="Life background"
              style={{ height: "35em" }}
            />
          </Grid>
        </Grid>

        <Grid item container className="" direction="column">
          <Grid
            item
            container
            className={classes.eventBackground}
            style={{ backgroundImage: `url(${aboutBg2})` }}
            justifyContent="flex-end"
          >
            <Grid
              item
              container
              md={6}
              direction="column"
              justifyContent="flex-end"
              className="c-cus-container"
              style={{ marginBottom: 50, paddingTop: "60px", paddingBottom: "60px"}}
              alignItems="center"
            >
              <Grid item>
                <div className={classes.culture}>
                  <span className="c-h5" style={{ fontFamily: "Gilroy-Bold" }}>
                    Culture & Event
                  </span>
                </div>
              </Grid>
              <Grid container item direction="column">
                <Grid
                  item
                  container
                  className={classes.event}
                  alignItems="center"
                >
                  <div
                    className={classes.iconContainer}
                    style={{ marginRight: 20 }}
                  >
                    <Icon component={FoodBankIcon} />
                  </div>
                  <span className="c-p">Free Snack</span>
                </Grid>
                <Grid
                  item
                  container
                  className={classes.event}
                  alignItems="center"
                >
                  <div
                    className={classes.iconContainer}
                    style={{ marginRight: 20 }}
                  >
                    <Icon component={FoodBankIcon} />
                  </div>
                  <span className="c-p">Company Trip</span>
                </Grid>
                <Grid
                  item
                  container
                  className={classes.event}
                  alignItems="center"
                >
                  <div
                    className={classes.iconContainer}
                    style={{ marginRight: 20 }}
                  >
                    <Icon component={FoodBankIcon} />
                  </div>
                  <span className="c-p">Beauty Care</span>
                </Grid>
                <Grid
                  item
                  container
                  className={classes.event}
                  alignItems="center"
                >
                  <div
                    className={classes.iconContainer}
                    style={{ marginRight: 20 }}
                  >
                    <Icon component={FoodBankIcon} />
                  </div>
                  <span className="c-p">Monthly Team Building</span>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item container className={[classes.eventBackground2]}>
            <Grid item container md={6} className={classes.boxContainer}>
              <Grid
                item
                container
                direction="column"
                className={classes.aboutBox}
              >
                <span className="c-h5 c-h5--maintain">ABOUT US</span>
                <span className="c-p">
                  Fetch will continue its good work to bridge the divide between
                  the World and the Vietnam Tech sector
                </span>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          container
          className="c-cus-container"
          style={{ paddingTop: 20, paddingLeft: "5%" }}
          rowSpacing={10}
        >
          <Grid item container>
            <span className="c-h4">VOICE FROM OUR EMPLOYEES </span>
          </Grid>
          <Grid item container justifyContent="center" columnSpacing={6} rowSpacing={6}>
            <Grid item container direction="column" md={4} sm={6}>
              <Grid item container justifyContent="center">
                <div className={classes.avatarContainer}></div>
              </Grid>
              <Grid item container direction="column">
                <span className="c-h5 c-h5--maintain text-center" style={{marginBottom: 20,}}>Minh Nguyen</span>
                <span className="c-h5 c-h5--maintain text-center" style={{marginBottom: 20,}}>DevDps Engineer</span>
              </Grid>
              <Grid item container direction="column">
                <span className="c-p text-center">
                  I feel very proud to be a member of Fetch Hanoi. The best
                  thing about working here is the teamwork I experience daily,
                  the valuable connections that I have
                </span>
              </Grid>
            </Grid>
            <Grid item container direction="column" md={4} sm={6}>
              <Grid item container justifyContent="center">
                <div className={classes.avatarContainer}></div>
              </Grid>
              <Grid item container direction="column">
                <span className="c-h5 c-h5--maintain text-center" style={{marginBottom: 20,}}>Minh Nguyen</span>
                <span className="c-h5 c-h5--maintain text-center" style={{marginBottom: 20,}}>DevDps Engineer</span>
              </Grid>
              <Grid item container direction="column">
                <span className="c-p text-center">
                  I feel very proud to be a member of Fetch Hanoi. The best
                  thing about working here is the teamwork I experience daily,
                  the valuable connections that I have
                </span>
              </Grid>
            </Grid>
            <Grid item container direction="column" md={4} sm={6}>
              <Grid item container justifyContent="center">
                <div className={classes.avatarContainer}></div>
              </Grid>
              <Grid item container direction="column">
                <span className="c-h5 c-h5--maintain text-center" style={{marginBottom: 20,}}>Minh Nguyen</span>
                <span className="c-h5 c-h5--maintain text-center" style={{marginBottom: 20,}}>DevDps Engineer</span>
              </Grid>
              <Grid item container direction="column">
                <span className="c-p text-center">
                  I feel very proud to be a member of Fetch Hanoi. The best
                  thing about working here is the teamwork I experience daily,
                  the valuable connections that I have
                </span>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </section>
  );
}

export default AboutUs;
