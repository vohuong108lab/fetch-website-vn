import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Slide from "@mui/material/Slide";
import { createStyles, makeStyles } from "@mui/styles";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import logo from "../../assets/images/logo.svg";
import { Link } from "react-router-dom";
import "../../assets/scss/header.scss";
import { IconButton, useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material";
import { SwipeableDrawer } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { List, ListItem, ListItemText } from "@mui/material";
function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

const useStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: "1.4rem",
    [theme.breakpoints.down("md")]: {
      marginBottom: "0.5rem",
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: "1rem",
    },
  },
  logo: {
    height: "3rem",
    [theme.breakpoints.down("md")]: {
      height: "2rem",
    },
  },
  logoContainer: {
    padding: "20px 0px",
  },
  tabContainer: {
    marginLeft: "auto",
  },
  tab: {
    marginLeft: "20px",
    textTransform: "none",
    fontWeight: "500",
    fontFamily: "Gilroy-Regular",
    fontSize: "1rem!important",
    color: "#FFBE16",
  },
  drawerIconContainer: {
    marginLeft: "auto!important",
    "&:hover": {
      backgroundColor: "transparent!important",
    },
  },
  drawer: {},

  drawerItem: {
    opacity: 0.7,
  },

  drawerItemSelected: {
    opacity: 1,
  },

  drawerItemText: {
    "& > span": { ...theme.typography.tab },
  },
  appbar: {
    zIndex: theme.zIndex.modal + 1 + "!important",
  },
}));

function Header(props) {
  const iOS =
    typeof navigator !== "undefined" &&
    /iPad|iPhone|iPod/.test(navigator.userAgent);
  const theme = useTheme();
  const [openDrawer, setOpenDrawer] = useState(false);
  const matches = useMediaQuery(theme.breakpoints.down("md"));
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const handleChange = (e, value) => {
    setValue(value);
  };
  useEffect(() => {
    if (window.location.pathname === "/" && value !== 0) {
      setValue(0);
    } else if (window.location.pathname === "/about" && value !== 1) {
      setValue(1);
    } else if (window.location.pathname === "/life" && value !== 2) {
      setValue(2);
    } else if (window.location.pathname === "/jobs" && value !== 3) {
      setValue(3);
    } else if (window.location.pathname === "/candidates" && value !== 4) {
      setValue(4);
    } else if (window.location.pathname === "/contact" && value !== 5) {
      setValue(4);
    }
  });
  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);
  const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
    setOpen(true);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setOpen(false);
  };

  const drawer = (
    <React.Fragment>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={openDrawer}
        onClose={() => {
          setOpenDrawer(false);
        }}
        onOpen={() => {
          setOpenDrawer(true);
        }}
        classes={{ paper: classes.drawer }}
      >
        <div className={classes.toolbarMargin} />
        <List disablePadding>
          <ListItem
            classes={
              value === 0
                ? { root: [classes.drawerItem, classes.drawerItemSelected] }
                : { root: classes.drawerItem }
            }
            selected={value === 0}
            onClick={() => {
              setOpenDrawer(false);
            }}
            divider
            button
            component={Link}
            to="/"
          >
            <ListItemText classes={{ root: classes.drawerItemText }}>
              Home
            </ListItemText>
          </ListItem>
          <ListItem
            classes={
              value === 1
                ? { root: [classes.drawerItem, classes.drawerItemSelected] }
                : { root: classes.drawerItem }
            }
            selected={value === 1}
            onClick={() => {
              setOpenDrawer(false);
            }}
            divider
            button
            component={Link}
            to="/about"
          >
            <ListItemText classes={{ root: classes.drawerItemText }}>
              About us
            </ListItemText>
          </ListItem>
          <ListItem
            classes={
              value === 2
                ? { root: [classes.drawerItem, classes.drawerItemSelected] }
                : { root: classes.drawerItem }
            }
            selected={value === 2}
            onClick={() => {
              setOpenDrawer(false);
            }}
            divider
            button
            component={Link}
            to="/life"
          >
            <ListItemText classes={{ root: classes.drawerItemText }}>
              Life at Fetch
            </ListItemText>
          </ListItem>
          <ListItem
            classes={
              value === 3
                ? { root: [classes.drawerItem, classes.drawerItemSelected] }
                : { root: classes.drawerItem }
            }
            selected={value === 3}
            onClick={() => {
              setOpenDrawer(false);
            }}
            divider
            button
            component={Link}
            to="/jobs"
          >
            <ListItemText classes={{ root: classes.drawerItemText }}>
              Jobs
            </ListItemText>
          </ListItem>
          <ListItem
            classes={
              value === 4
                ? { root: [classes.drawerItem, classes.drawerItemSelected] }
                : { root: classes.drawerItem }
            }
            selected={value === 4}
            onClick={() => {
              setOpenDrawer(false);
            }}
            divider
            button
            component={Link}
            to="/candidates"
          >
            <ListItemText classes={{ root: classes.drawerItemText }}>
              Refer candidates
            </ListItemText>
          </ListItem>
          <ListItem
            classes={
              value === 5
                ? { root: [classes.drawerItem, classes.drawerItemSelected] }
                : { root: classes.drawerItem }
            }
            selected={value === 5}
            onClick={() => {
              setOpenDrawer(false);
            }}
            divider
            button
            component={Link}
            to="/contact"
          >
            <ListItemText classes={{ root: classes.drawerItemText }}>
              Contact us
            </ListItemText>
          </ListItem>
        </List>
      </SwipeableDrawer>
      <IconButton
        onClick={() => {
          setOpenDrawer(!openDrawer);
        }}
        disableRipple
        className={classes.drawerIconContainer}
      >
        <MenuIcon />
      </IconButton>
    </React.Fragment>
  );
  const tabs = (
    <React.Fragment>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="white"
        className={classes.tabContainer}
      >
        <Tab
          className="tab"
          component={Link}
          to="/"
          label="Home"
          variant="header"
        />
        <Tab
          className="tab"
          component={Link}
          to="/about"
          label="About us"
          variant="header"
          variant="header"
        />
        <Tab
          className="tab"
          component={Link}
          to="/life"
          label="Life at Fetch"
          variant="header"
        />
        <Tab
          className="tab"
          component={Link}
          to="/jobs"
          label="Jobs"
          variant="header"
        />
        <Tab
          className="tab"
          component={Link}
          to="/candidates"
          label="Refer candidates"
          variant="header"
        />

        <Tab
          className="tab"
          component={Link}
          to="/contact"
          label="Contact us"
          variant="header"
        />
      </Tabs>
    </React.Fragment>
  );
  return (
    <React.Fragment>
      <HideOnScroll {...props}>
        <AppBar
          classes={{ root: classes.appbar }}
          elevation={0}
          position="fixed"
          color="white"
        >
          <Toolbar>
            <Link to="/">
              <div item className={classes.logoContainer}>
                <img className={classes.logo} src={logo} alt="company logo" />
              </div>
            </Link>

            {matches ? drawer : tabs}
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <div className={classes.toolbarMargin} />
    </React.Fragment>
  );
}

export default Header;
