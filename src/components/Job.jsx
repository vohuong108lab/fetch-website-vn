import {
  Chip,
  Grid,
  Button,
  useMediaQuery,
  Divider,
  Icon,
} from "@mui/material";
import { grey, teal } from "@mui/material/colors";
import { makeStyles, useTheme } from "@mui/styles";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Form, Input, Select, Upload } from "antd";
import { Modal } from "antd";
import { SentimentVerySatisfied, UploadOutlined } from "@mui/icons-material";
import ReCAPTCHA from "react-google-recaptcha";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import GoogleMapReact from "google-map-react";
const useStyles = makeStyles((theme) => ({
  header: {
    background: `linear-gradient(${theme.palette.yellow.light},${theme.palette.secondary.main})`,
  },
  inputBox: {
    maxWidth: 400,
    border: "2px solid black",
    borderRadius: "10px",
    boxShadow: "0px 10px 15px rgba(0,0,0,0.2)",
  },
  iconContainer: {
    padding: "2px 10px",
    background: `linear-gradient(to bottom,black, ${theme.palette.grey.dark}, black)`,
    display: "flex",
    alignItems: "center",
    "& svg": {
      color: theme.palette.secondary.main,
      fontSize: "30px",
    },
  },
  imgContainer: {
    width: "100%",
    maxHeight: "40em",
    overflow: "hidden",
    "& img": {
      maxWidth: "100%",
      flexGrow: 1,
    },
  },
  steps: {
    marginBottom: "3em!important",
    [theme.breakpoints.up("md")]: {
      transform: "translateY(-60%)",
      marginBottom: "0!important",
    },
  },
  step: {
    padding: "20px 50px",
    [theme.breakpoints.down("lg")]: {
      padding: "20px 30px",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: "2em!important",
      borderRadius: 10,
    },
  },
  stepImg: {
    width: 86,
    height: 86,
    borderRadius: "50%",
    backgroundColor: "white",
    marginBottom: 10,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& img": {
      width: "50%",
    },
    [theme.breakpoints.down("sm")]: {
      width: 54,
      height: 54,
    },
  },
  chip: {
    marginRight: "5px!important",
    borderRadius: "5px!important",
    "& .MuiChip-label": {
      fontFamily: "Gilroy-Bold",
    },
  },
  seeMore: {
    fontFamily: " Gilroy-SemiBold",
    fontSize: 17,
    lineHeight: "145%",
    color: teal[300],
    "&:hover": {
      color: teal[900],
    },
  },
  button: {},
  applyButton: {
    "& .MuiButton-root": {
      borderRadius: "3em",
      fontFamily: "Gilroy-SemiBold",
      minWidth: 200,
    },
  },
  questionNum: {
    height: 80,
    width: 80,
    backgroundColor: theme.palette.secondary.main,
    borderRadius: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "1em",
  },
  modalBox: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  },
  modalContainer: {
    padding: "20px 5%",
    [theme.breakpoints.down("md")]: {
      padding: "20px 16px",
    },
  },
  form: {
    "& .ant-form-item": {
      flexGrow: "1!important",
      flexDirection: "column",
      "& .ant-form-item-label,& label": {
        textAlign: "start",
        fontSize: "1rem",
        fontFamily: "Gilroy-Regular",
      },
      marginRight: "32px",
    },
  },
  title: {
    color: theme.palette.secondary.main,
    fontFamily: "Gilroy-SemiBold",
    fontSize: "16px",
    marginBottom: 8,
    lineHeight: "1.6"
  },
  subTitle:{
    fontFamily: "Gilroy-SemiBold",
    fontSize: "16px",
    marginBottom: 8,
    lineHeight: "1.5"
  },
  paragraph:{
      marginBottom: "1rem", 
      fontFamily: "Gilroy-Regular",
      fontSize: 16
  }
}));
function Job({
  title,
  urgent,
  areas,
  description,
  office,
  minSalary,
  maxSalary,
  type,
  id,
}) {
  const theme = useTheme();
  const classes = useStyles();

  const matchesMD = useMediaQuery(theme.breakpoints.down("md"));
  const matchesLG = useMediaQuery(theme.breakpoints.up("md"));
  const matchesSM = useMediaQuery(theme.breakpoints.down("sm"));
  const [verified, setVerivied] = useState(false);

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onCapchaChange = () => {
    setVerivied(true);
  };
  const sendApplyForm = () => {};
  const [modalOpen, setModalOpen] = useState(false);
  const openModal = () => {
    setModalOpen(true);
  };
  const closeModal = () => {
    setModalOpen(false);
  };
  const { Option } = Select;
  const modal = (
    <Modal
      aria-labelledby="unstyled-modal-title"
      aria-describedby="unstyled-modal-description"
      visible={modalOpen}
      footer={null}
      onCancel={closeModal}
      zIndex={1303}
      width={800}
    >
      <Grid container className={classes.modalContainer}>
        <Grid
          className={classes.applyHeader}
          item
          container
          direction="column"
          style={{ marginBottom: "2rem" }}
        >
          <div
            className="c-h3 "
            style={{
              borderBottom: `8px solid ${theme.palette.secondary.main}`,
              lineHeight: "18px",
              marginBottom: "1rem",
              marginTop: "1rem",
            }}
          >
            Application Form
          </div>
          <div className="c-h3" style={{ fontFamily: "Gilroy-Regular" }}>
            Let us know more about you.
          </div>
        </Grid>
        <Grid className={classes.form} item container>
          <Grid container item md={6}>
            <Form.Item
              label="Full name"
              name="fullname"
              rules={[
                { required: true, message: "Please input your full name!" },
              ]}
            >
              <Input />
            </Form.Item>
          </Grid>
          <Grid container item md={6}>
            <Form.Item
              label="Email"
              name="email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input />
            </Form.Item>
          </Grid>
          <Grid container item md={6}>
            <Form.Item
              label="Phone number"
              name="phone"
              rules={[
                {
                  required: true,
                  message: "Please input your Phone number!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Grid>
          <Grid container item md={6}>
            <Form.Item
              name="cv"
              label="Curriculum Vitae"
              valuePropName="fileList"
              rules={[{ required: true, message: "Please upload your CV!" }]}
            >
              <Upload name="logo" action="/upload.do" listType="picture">
                <button>choose file</button>
              </Upload>
            </Form.Item>
          </Grid>
          <Grid container item md={12}>
            <Form.Item
              label="Skills"
              name="skills"
              rules={[{ required: false, message: "Please input your email!" }]}
            >
              <Select
                getPopupContainer={() =>
                  document.querySelector(".ant-modal-content")
                }
                placeholder="Select skills"
                allowClear
              >
                <Option value="ReactJs">ReactJs</Option>
                <Option value="SQL">SQL</Option>
              </Select>
            </Form.Item>
          </Grid>
          <Grid container item md={12}>
            <Form.Item
              label="Your message"
              name="message"
              rules={[
                {
                  required: false,
                  message: "Please input your Phone number!",
                },
              ]}
            >
              <Input.TextArea rows={6} />
            </Form.Item>
          </Grid>
          <Grid container item md={6}>
            <ReCAPTCHA
              sitekey="6LfNgp0cAAAAADu-4371jDX2JQgT1aPsaD6opNIK"
              onChange={onCapchaChange}
            />
            ,
          </Grid>
          <Grid
            container
            item
            md={6}
            alignItems="center"
            justifyContent="center"
          >
            <Button
              sx={{ textTransform: "none" }}
              variant="contained"
              color="secondary"
              onClick={sendApplyForm}
            >
              <span
                className="c-p"
                style={{ fontFamily: "Gilroy-SemiBold", color: "white" }}
              >
                Apply now
              </span>{" "}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Modal>
  );
  return (
    <Grid container direction="column" style={{ textAlign: "start" }}>
      {modal}
      <Grid
        item
        container
        columnSpacing={3}
        className="c-cus-container"
        style={{ paddingTop: 30, paddingBottom: 30 }}
      >
        <Grid
          item
          container
          md={9}
          xs={12}
          rowSpacing={2}
          style={{ marginBottm: "2em" }}
        >
          <Grid item container direction="row" flexWrap="wrap">
            <div
              className="c-h4"
              style={{ marginRight: "5px", fontFamily: "Gilroy-Regular" }}
            >
              {title}
            </div>
            <div>
              {urgent && (
                <Chip
                  className={classes.chip}
                  classes={{ label: classes.chipLabel }}
                  label="Urgent"
                  color="error"
                  clickable
                />
              )}
              {areas?.length > 0 &&
                areas.map((area) => (
                  <Chip
                    className={classes.chip}
                    classes={{ label: classes.chipLabel }}
                    label={area}
                    color="info"
                    clickable
                  />
                ))}
            </div>
          </Grid>
          {/* <Grid item container>
            <div className="c-p" style={{ textAlign: "start" }}>
              {description}
            </div>
          </Grid>
          <Grid item container>
            <Link className={classes.seeMore} to="/jobs-details">
              See more
            </Link>
          </Grid> */}

          <Grid item container>
            <div className="c-p" style={{ color: grey[500] }}>
              {type}
            </div>
          </Grid>
          <Grid item container md={8}>
            <Grid
              item
              container
              sm
              style={{ marginBottom: "1em", marginLeft: "-8px" }}
            >
              <Icon component={AttachMoneyIcon} color="secondary" />
              <div className="c-p" style={{ color: grey[500] }}>
                {minSalary} - {maxSalary} USD
              </div>
            </Grid>
            <Grid
              item
              container
              sm
              style={{ marginBottom: "1em", marginLeft: "-8px" }}
            >
              <Icon component={LocationOnIcon} color="secondary" />
              <div className="c-p" style={{ color: grey[500] }}>
                {office}
              </div>
            </Grid>
          </Grid>
        </Grid>

        {matchesLG && <Grid item md={1} xs={0}></Grid>}

        <Grid
          item
          md={2}
          container
          alignItems={matchesLG && "center"}
          className={classes.applyButton}
        >
          <Button
            variant="contained"
            fullWidth={matchesLG}
            color="secondary"
            onClick={openModal}
            sx={{ textTransform: "none" }}
          >
            <span
              className=""
              style={{
                color: "white",
                fontFamily: "Gilroy-SemiBold",
                fontSize: "",
              }}
            >
              Apply Now
            </span>
          </Button>
        </Grid>
      </Grid>
      <Grid item sm={12} xs={12}>
        <Divider
          style={{
            border: ".5px solid #e2e2e2",
            boxShadow: "0 1px 2px #e2e2e2",
          }}
        ></Divider>
      </Grid>
      <Grid item container className="c-cus-container" columnSpacing={6}>
        <Grid item container direction="column" md={8}>
          <div className={classes.title}>1. INSTRUCTION</div>
          <div className={classes.subTitle}>a. About Fetch:</div>
          <div className={classes.paragraph}>
            Fetch Technology Vietnam is a comprehensive global provider of HR
            and Talent Acquisition Services, focusing primarily in the
            technology fields. Founded in 2016, Fetch Technology Vietnam helps
            foreign companies of all types and sizes reach their potential by
            providing the talent and support to efficiently build and scale a
            high-performing, distributed workforce in Vietnam.
          </div>
          <div className={classes.paragraph}>
            Our mission is to offer Vietnam’s most talented technologists a
            platform to connect with some of the world’s leading tech companies
            and build their expertise on a global scale. Over 4 years, Fetch has
            built a good reputation and is trusted by many Vietnamese and
            foreign companies; And Fetch will continue its good work to bridge
            the divide between the World and the Vietnam Tech sector.
          </div>
          <div className={classes.subTitle}>b. About Client:</div>
          <div className={classes.paragraph}>
            Our client is the preferred financing partner in supporting
            Vehicle/COE Financing, floor stocking, equipment financing, accounts
            receivable financing/factoring, loans for industrial/commercial
            property and property development, renovation loans, and other forms
            of commercial lending. They are planning to steadily expand their
            range of financial services to cater for more of customer’s
            financing needs.
          </div>

          <div className={classes.title}>4. WHY YOU‘LL LOVE WORKING HERE</div>
          <div className={classes.subTitle}>a. About Fetch:</div>
          <div className={classes.paragraph}>
            Being a Jr. ReactJS Developer at Fetch Technology, you will
            experience a youthful, flexible and creative work environment:
          </div>
          <ul>
            <li>
              Working in a professional, friendly, well-equipped environment
              both with seniors, foreigners and Vietnamese.
            </li>
            <li>
              Extensive on job training, you will always have a chance to work
              with new emerging technologies.
            </li>
            <li>
              Full social insurance, health insurance & unemployment insurance
              according to Vietnam Labor Law.
            </li>
            <li>12-day annual leave per year.</li>
          </ul>
          <div className={classes.title}>5. WORKING TIME & LOCATION</div>
          <ul>
            <li>Working time: 9AM - 6PM from Monday to Friday.</li>
            <li>
              Location: 3rd Floor, BCONS TOWER 2 - 42/1 Ung Van Khiem Street,
              Ward 25, Binh Thanh District, Ho Chi Minh City.
            </li>
          </ul>
        </Grid>
        <Grid item container md={4} direction="column" rowSpacing={5}>
          <Grid item container direction="column">
            <div className="c-h5">Skills required</div>
            <Grid item container>
              <div className="catalog-job">JavaScript</div>
              <div className="catalog-job">JavaScript</div>
              <div className="catalog-job">JavaScript</div>
              <div className="catalog-job">JavaScript</div>
            </Grid>
          </Grid>
          <Grid item container direction="column">
            <div className="c-h5">Location</div>
            <Grid item style={{ height: 300 }}>
              <GoogleMapReact
                bootstrapURLKeys={{
                  key: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDd7UzwMV1g9caymZSBwWm8xhHbTBGgJCM&callback=initMap",
                }}
                defaultZoom={20}
                yesIWantToUseGoogleMapApiInternal
                defaultCenter={{ lat: 21.031658, lng: 105.782762 }}
              >
                <div lat={59.955413} lng={30.337844} text="My Marker">
                  My Marker
                </div>
              </GoogleMapReact>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Job;
