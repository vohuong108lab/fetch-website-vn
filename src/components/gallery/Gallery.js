import React from 'react';
import '../../assets/scss/Gallery.scss';
import ReactPlayer from 'react-player';
import { Row, Col, Image  } from 'antd';
import gallery1 from '../../assets/img/gallery1.JPG';
import gallery2 from '../../assets/img/gallery2.JPG';
import gallery3 from '../../assets/img/gallery3.JPG';
import gallery4 from '../../assets/img/gallery4.JPG';
import gallery5 from '../../assets/img/gallery5.JPG';
import gallery6 from '../../assets/img/gallery6.JPG';
import gallery7 from '../../assets/img/gallery7.JPG';
import gallery8 from '../../assets/img/gallery8.JPG';
import gallery9 from '../../assets/img/gallery9.JPG';
import locationImg from '../../assets/img/gyt-min1.jpg';
import { ReactComponent as FacebookIcon } from '../../assets/img/facebook.svg';
import { ReactComponent as TiktokIcon } from '../../assets/img/tiktok.svg';
import { ReactComponent as LinkedinIcon } from '../../assets/img/linkedin.svg';
import { Link } from 'react-router-dom';

const imgData = [gallery1, gallery2, gallery3, gallery4, gallery5, gallery6, gallery7, gallery8, gallery9 ];
const socialData = [FacebookIcon, TiktokIcon, LinkedinIcon];

const Gallery = () => {

    return (
        <div className="gallery">
            <section className="fetch-video">
                <div className="cus-container">
                    <Row gutter={[{xs: 21, sm: 24, xl:30}, 0]}>
                        <Col span={24}>
                            <div className="player-wrapper">
                                <ReactPlayer 
                                    className="react-player" 
                                    controls={true} 
                                    url='https://www.youtube.com/watch?v=m82NjSglrpc' 
                                    width='100%'
                                    height='100%'
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            <section className="fetch-image">
                <div className="cus-container">
                    <h2>Our Awesome Moments</h2>
                    <Row gutter={[{xs: 21, sm: 24, xl:30}, 30]}>
                        {imgData.map((item, index) => (
                            <Col key={index} xs={24} sm={12} lg={8}>
                                <div className="img-wrap">
                                    <Image width="100%" src={item} alt="fetch img" />
                                    {/* <img src={item} alt="fetch img"/> */}
                                </div>
                            </Col>
                        ))}
                    </Row>
                </div>
            </section>
            <section className="fetch-location">
                <div className="cus-container">
                    <Row className="row-location" gutter={[{xs: 21, sm: 24, xl:30}, 0]} justify="space-between">
                        <Col xs={24} sm={24} md={10} xl={9}>
                            <div className="content-wrap">
                                <h3>LOCATION</h3>
                                <p>Ho Chi Minh: BCONS 2 Level 3, 42/1 Ung Van Khiem Street, Ward 25, Binh Thanh District</p>
                                <p>Ha Noi: Lane 82, Duy Tan Street, Dich Vong Hau Ward, Cau Giay District</p>
                            </div>
                            <div className="social-wrap">
                                <h3>FOLLOW US</h3>
                                <div className="list-icon">
                                    {socialData.map((Item,index) => (
                                        <Link key={index}>
                                            <div className="icon-wrap">
                                                <Item class="social-icon"/>
                                            </div>
                                        </Link>
                                    ))}
                                </div>
                                <p>©2021 Privacy policy</p>
                            </div>
                        </Col>
                        <Col xs={24} sm={24} md={14} xl={13}>
                            <div className="img-wrap">
                                <img src={locationImg} alt="location"/>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
        </div>
    )
}

export default Gallery
