export const jobs = [
    {
      title: "HCM - Junior Fullstack Developer (500-900 USD)",
      urgent: true,
      areas: ["HCM"],
      description:
        "A leading brand name in Singapore for Industrial Vehicles, headquartered in Singapore and has operations in Malaysia and Vietnam",
      office: "Ho Chi Minh Office",
      minSalary: 500,
      maxSalary: 900,
      type: "fulltime",
      id: 1,
    },
    {
      title: "HCM - Junior Fullstack Developer (500-900 USD)",
      urgent: true,
      areas: ["HCM"],
      description:
        "A leading brand name in Singapore for Industrial Vehicles, headquartered in Singapore and has operations in Malaysia and Vietnam",
      office: "Ho Chi Minh Office",
      minSalary: 500,
      maxSalary: 900,
      type: "fulltime",
      id: 2,
    },
    {
      title: "HCM - Junior Fullstack Developer (500-900 USD)",
      urgent: true,
      areas: ["HCM"],
      description:
        "A leading brand name in Singapore for Industrial Vehicles, headquartered in Singapore and has operations in Malaysia and Vietnam",
      office: "Ho Chi Minh Office",
      minSalary: 500,
      maxSalary: 900,
      type: "fulltime",
      id: 3,
    },
  ];