import React, { useState } from "react";
import { Link } from "react-router-dom";
import { makeStyles, useTheme } from "@mui/styles";
import {
  Button,
  Grid,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import phoneIcon from "../assets/images/phone.svg";
import emailIcon from "../assets/images/email.svg";

import contactBackground from "../assets/images/contactBackground.jpg";
import airplane from "../assets/images/send.svg";
import PhoneIcon from "@mui/icons-material/Phone";
import EmailIcon from "@mui/icons-material/Email";
const useStyles = makeStyles((theme) => ({
  background: {
    backgroundImage: `url(${contactBackground})`,
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    height: "30em",
  },
  message: {
    marginTop: "5em!important",
  },
  sendButton: {
    borderRadius: "50px!important",
    color: "white!important",
    height: 45,
    width: 245,
    fontSize: "1rem!important",
    fontFamily: "Gilroy-SemiBold!important",
    backgroundColor: `${theme.palette.black.main}!important`,
    marginTop: "1em!important",
    "&:hover": {
      backgroundColor: `${theme.palette.common.grey2}!important`,
    },
  },
  formGroup: {
    background: `#FFFFFF`,
    border: ` 1px solid #DEE7F0`,
    boxSizing: `border-box`,
    /* Shadow 04 */

    boxShadow: `0px 30px 26px rgba(39, 58, 95, 0.15)`,
    borderRadius: `12px`,
    padding: "30px",
    width: "80%"
  },
}));

function ContactUs() {
  const classes = useStyles();
  const theme = useTheme();
  const matchesLG = useMediaQuery(theme.breakpoints.up("xl"));
  const matchesMD = useMediaQuery(theme.breakpoints.down("lg"));
  const matchesSM = useMediaQuery(theme.breakpoints.down("md"));
  const matchesXS = useMediaQuery(theme.breakpoints.down("sm"));
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const onNameChange = (e) => {
    setName(e.target.value);
  };

  const onEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const onPhoneChange = (e) => {
    setPhone(e.target.value);
  };
  const onMessageChange = (e) => {
    setMessage(e.target.value);
  };
  return (
    <Grid container direction="row" style={{ padding: "50px 20px", overflowX: "hidden" }}>
      <Grid
        item
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        lg={6}
      >
        <Grid item className={classes.formGroup} style={{width: matchesXS?"95%":matchesSM?"80%":matchesMD?"60%":matchesLG?"55%":"70%"}}>
          <Grid item>
            <div
              className="c-h2"
              style={{
                textAlign: matchesMD && "center",
                color: `${theme.palette.common.black}`,
              }}
            >
              Contact Us
            </div>
            <div
              className="c-p black"
              style={{
                textAlign: matchesMD && "center",
                color: `${theme.palette.common.black}`,
              }}
            >
              We re waiting
            </div>
          </Grid>
          <Grid item container style={{ marginTop: "2em" }}>
            <Grid item>
              <PhoneIcon
                style={{ marginRight: "0.5em", verticalAlign: "bottom" }}
              />
            </Grid>
            <Grid item>
              <div className="c-p black">(5555) 5555</div>
            </Grid>
          </Grid>
          <Grid item container>
            <Grid item>
              <EmailIcon
                style={{ marginRight: "0.5em", verticalAlign: "bottom" }}
              />
            </Grid>
            <Grid item>
              <div className="c-p black">dtluat@gmail.com </div>
            </Grid>
          </Grid>
          <Grid
            item
            container
            direction="column"
            style={{  marginTop: "2em" }}
          >
            <Grid item>
              <TextField
                fullWidth
                label="Name"
                id="name"
                color="black"
                variant="standard"
                value={name}
                onChange={onNameChange}
              />
            </Grid>
            <Grid item>
              <TextField
                fullWidth
                label="Email"
                id="email"
                color="black"
                variant="standard"
                value={email}
                onChange={onEmailChange}
              />
            </Grid>
            <Grid item style={{ }}>
              <TextField
                fullWidth
                label="Phone"
                id="phone"
                color="black"
                variant="standard"
                value={phone}
                onChange={onPhoneChange}
              />
            </Grid>
          </Grid>
          <Grid item>
            <TextField
              fullWidth
              value={message}
              multiline
              rows={10}
              id="message"
              onChange={onMessageChange}
              variant="outlined"
              color="black"
              className={classes.message}
              label="Send us a message..."
            ></TextField>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justifyContent="center"
            style={{ marginBottom: "2em" }}
          >
            <Button classes={{ root: classes.sendButton }} variant="contained">
              Send message
              <img
                src={airplane}
                style={{ marginLeft: "0.5em" }}
                alt="airplane"
              ></img>
            </Button>
          </Grid>
        </Grid>
      </Grid>

      <Grid item container alignItems="center" lg={6}>
        {!matchesSM&&
        <img style={{}} src={contactBackground} alt="background" />}
      </Grid>
    </Grid>
  );
}

export default ContactUs;
