import React, { useState } from "react";
import { Button, Chip, Grid, useMediaQuery } from "@mui/material";
import { useTheme, makeStyles } from "@mui/styles";
import { Typewriter, useTypewriter, Cursor } from "react-simple-typewriter";
import Icon from "@mui/material/Icon";
import SearchIcon from "@mui/icons-material/Search";
import jobsBg1 from "../assets/images/jobsBg1.jpg";
import { Box, flexbox, styled } from "@mui/system";
import { cyan, grey, purple, red, teal, yellow } from "@mui/material/colors";
import computer from "../assets/images/computer.svg";
import interview from "../assets/images/interview.svg";
import test from "../assets/images/test.svg";
import meeting from "../assets/images/meeting.svg";
import offer from "../assets/images/offer.svg";
import { Link } from "react-router-dom";
import Divider from "@mui/material/Divider";
import { Form, Input, Select, Upload } from "antd";
import { Modal } from "antd";
import { SentimentVerySatisfied, UploadOutlined } from "@mui/icons-material";
import ReCAPTCHA from "react-google-recaptcha";
import { jobs } from "./data/jobs";

const jobsList = jobs
const faqs = [
  {
    question: "Which language is preferable in hiring process at Fetch",
  },
  {
    question: "Which language is preferable in hiring process at Fetch",
  },
  {
    question: "Which language is preferable in hiring process at Fetch",
  },
  {
    question: "Which language is preferable in hiring process at Fetch",
  },
  {
    question: "Which language is preferable in hiring process at Fetch",
  },
];

const useStyles = makeStyles((theme) => ({
  header: {
    background: `linear-gradient(${theme.palette.yellow.light},${theme.palette.secondary.main})`,
  },
  inputBox: {
    maxWidth: 400,
    border: "2px solid black",
    borderRadius: "10px",
    boxShadow: "0px 10px 15px rgba(0,0,0,0.2)",
  },
  iconContainer: {
    padding: "2px 10px",
    background: `linear-gradient(to bottom,black, ${theme.palette.grey.dark}, black)`,
    display: "flex",
    alignItems: "center",
    "& svg": {
      color: theme.palette.secondary.main,
      fontSize: "30px",
    },
  },
  imgContainer: {
    width: "100%",
    maxHeight: "40em",
    overflow: "hidden",
    "& img": {
      maxWidth: "100%",
      flexGrow: 1,
    },
  },
  steps: {
    marginBottom: "3em!important",
    [theme.breakpoints.up("md")]: {
      transform: "translateY(-60%)",
      marginBottom: "0!important",
    },
  },
  step: {
    padding: "20px 50px",
    [theme.breakpoints.down("lg")]: {
      padding: "20px 30px",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: "2em!important",
      borderRadius: 10,
    },
  },
  stepImg: {
    width: 86,
    height: 86,
    borderRadius: "50%",
    backgroundColor: "white",
    marginBottom: 10,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& img": {
      width: "50%",
    },
    [theme.breakpoints.down("sm")]: {
      width: 54,
      height: 54,
    },
  },
  chip: {
    marginRight: "5px!important",
    borderRadius: "5px!important",
    "& .MuiChip-label": {
      fontFamily: "Gilroy-Bold",
    },
  },
  seeMore: {
    fontFamily: " Gilroy-SemiBold",
    fontSize: 17,
    lineHeight: "145%",
    color: teal[300],
    "&:hover": {
      color: teal[900],
    },
  },
  button: {},
  applyButton: {
    "& .MuiButton-root": {
      borderRadius: "3em",
      fontFamily: "Gilroy-SemiBold",
      minWidth: 200,
    },
  },
  questionNum: {
    height: 80,
    width: 80,
    backgroundColor: theme.palette.secondary.main,
    borderRadius: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "1em",
  },
  modalBox: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  },
  modalContainer: {
    padding: "20px 5%",
    [theme.breakpoints.down("md")]: {
      padding: "20px 16px",
    },
  },
  form: {
    "& .ant-form-item": {
      flexGrow: "1!important",
      flexDirection: "column",
      "& .ant-form-item-label,& label": {
        textAlign: "start",
        fontSize: "1rem",
        fontFamily: "Gilroy-Regular"
      },
      marginRight: "32px",
    },
  },
}));

function Jobs() {
  const theme = useTheme();
  const classes = useStyles();
  const matchesMD = useMediaQuery(theme.breakpoints.down("md"));
  const matchesLG = useMediaQuery(theme.breakpoints.up("md"));
  const matchesSM = useMediaQuery(theme.breakpoints.down("sm"));
  const { text, count } = useTypewriter({
    words: ["WE ARE HIRING "],
    loop: 1,
    cursor: true,
  });
  const [modalOpen, setModalOpen] = useState(false);
  const openModal = () => {
    setModalOpen(true);
  };
  const closeModal = () => {
    setModalOpen(false);
  };
  const Job = ({
    title,
    urgent,
    areas,
    description,
    office,
    minSalary,
    maxSalary,
    type,
    id
  }) => {
    return (
      <React.Fragment>
        <Grid item container columnSpacing={3}>
          <Grid
            item
            container
            lg={8}
            xs={12}
            rowSpacing={2}
            style={{ marginBottm: "2em" }}
          >
            <Grid item container direction="row" flexWrap="wrap">
              <div className="c-p c--bold" style={{ marginRight: "5px" }}>
                {title}
              </div>
              <div>
                {urgent && (
                  <Chip
                    className={classes.chip}
                    classes={{ label: classes.chipLabel }}
                    label="Urgent"
                    color="error"
                    clickable
                  />
                )}
                {areas?.length > 0 &&
                  areas.map((area) => (
                    <Chip
                      className={classes.chip}
                      classes={{ label: classes.chipLabel }}
                      label={area}
                      color="info"
                      clickable
                    />
                  ))}
              </div>
            </Grid>
            <Grid item container>
              <div className="c-p" style={{ textAlign: "start" }}>
                {description}
              </div>
            </Grid>
            <Grid item container>
              <Link className={classes.seeMore} to={'/jobs-details/' + id}>
                See more
              </Link>
            </Grid>
          </Grid>
          <Grid item container lg={2}>
            <Grid
              item
              container
              direction="column"
              style={{ marginBottom: "2em" }}
            >
              <div className="c-p c--bold">{office}</div>
              <div className="c-p" style={{ color: grey[500] }}>
                {minSalary} - {maxSalary} USD
              </div>
              <div className="c-p" style={{ color: grey[500] }}>
                {type}
              </div>
            </Grid>
          </Grid>
          <Grid item lg={2} className={classes.applyButton}>
            <Button
              variant="contained"
              fullWidth={matchesLG || matchesSM}
              color="secondary"
              onClick={openModal}
            >
              APPLY NOW
            </Button>
          </Grid>
          <Grid item sm={10} xs={12}>
            <Divider
              style={{ marginTop: "3em", marginBottom: "1em" }}
            ></Divider>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  };
  const [verified, setVerivied] = useState(false);

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onCapchaChange = () => {
    setVerivied(true)
  };
  const sendApplyForm = () =>{

  }
  const { Option } = Select;
  return (
    <Grid container style={{ textAlign: "start" }}>
      <Modal
        aria-labelledby="unstyled-modal-title"
        aria-describedby="unstyled-modal-description"
        visible={modalOpen}
        footer={null}
        onCancel={closeModal}
        zIndex={1303}
        width={800}
      >
        <Grid container className={classes.modalContainer}>
          <Grid
            className={classes.applyHeader}
            item
            container
            direction="column"
            style={{marginBottom: "2rem"}}
          >
            <div
              className="c-h3 "
              style={{
                borderBottom: `8px solid ${theme.palette.secondary.main}`,
                lineHeight: "18px",
                marginBottom: "1rem",
                marginTop: "1rem",
              }}
            >
              Application Form
            </div>
            <div className="c-h3" style={{ fontFamily: "Gilroy-Regular" }}>
              Let us know more about you.
            </div>
          </Grid>
          <Grid className={classes.form} item container>
            <Grid container item md={6}>
              <Form.Item
                label="Full name"
                name="fullname"
                rules={[
                  { required: true, message: "Please input your full name!" },
                ]}
              >
                <Input />
              </Form.Item>
            </Grid>
            <Grid container item md={6}>
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  { required: true, message: "Please input your email!" },
                ]}
              >
                <Input />
              </Form.Item>
            </Grid>
            <Grid container item md={6}>
              <Form.Item
                label="Phone number"
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Please input your Phone number!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Grid>
            <Grid container item md={6}>
              <Form.Item
                name="cv"
                label="Curriculum Vitae"
                valuePropName="fileList"
                rules={[{ required: true, message: "Please upload your CV!" }]}
              >
                <Upload name="logo" action="/upload.do" listType="picture">
                  <button>choose file</button>
                </Upload>
              </Form.Item>
            </Grid>
            <Grid container item md={12}>
              <Form.Item
                label="Skills"
                name="skills"
                rules={[
                  { required: false, message: "Please input your email!" },
                ]}
              >
                <Select
                  getPopupContainer={() =>
                    document.querySelector(".ant-modal-content")
                  }
                  placeholder="Select skills"
                  allowClear
                >
                  <Option value="ReactJs">ReactJs</Option>
                  <Option value="SQL">SQL</Option>
                </Select>
              </Form.Item>
            </Grid>
            <Grid container item md={12}>
              <Form.Item
                label="Your message"
                name="message"
                rules={[
                  {
                    required: false,
                    message: "Please input your Phone number!",
                  },
                ]}
              >
                <Input.TextArea rows={6} />
              </Form.Item>
            </Grid>
            <Grid container item md={6}>
              <ReCAPTCHA
                sitekey="6LfNgp0cAAAAADu-4371jDX2JQgT1aPsaD6opNIK"
                onChange={onCapchaChange}
              />
              ,
            </Grid>
            <Grid container item md={6} alignItems="center" justifyContent="center">
              <Button
                sx={{ textTransform: "none" }}
                variant="contained"
                color="secondary"
                onClick={sendApplyForm}
              >
                <span className="c-p" style={{fontFamily: "Gilroy-SemiBold", color: "white"}}>Apply now</span>{" "}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Modal>
      <Grid item container className={[classes.header, "c-header-container"]}>
        <Grid item container md={6}>
          <Grid
            item
            container
            className={classes.inputBox}
            direction="row"
            justifyContent="space-between"
          >
            <Grid item alignItems="flex-end" style={{ padding: 10 }}>
              <span
                className="c-p grey-1"
                style={{
                  fontFamily: "Gilroy-SemiBold",
                  verticalAlign: "baseline",
                  fontSize: "16px",
                  lineHeight: "20px",
                
                }}
              >
                {text}
              </span>
              <Cursor style={{ height: 100 }} />
            </Grid>
            <Grid item className={classes.iconContainer}>
              <Icon color="white" component={SearchIcon} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item container md={6}></Grid>
      </Grid>
      <Grid item container direction="column">
        <Grid item container justifyContent="center" style={{ padding: 30 }}>
          <span className="c-h3 text-center c--bold">Hiring process</span>
        </Grid>
        <Grid item container>
          <Grid item container className={classes.imgContainer}>
            <img src={jobsBg1} alt="" />
          </Grid>
          <Grid
            item
            container
            justifyContent="center"
            className={classes.steps}
          >
            <Grid
              item
              container
              md={2}
              sm={4}
              xs={8}
              className={classes.step}
              direction="column"
              justifyContent="flex-start"
              style={{ backgroundColor: purple[900] }}
              alignItems="center"
            >
              <Grid
                item
                flexGrow="0"
                justifyContent="center"
                alignItems="center"
              >
                <div className={classes.stepImg}>
                  <img src={computer} alt="" />
                </div>
              </Grid>
              <Grid item>
                <div className="c-p text-center" style={{ color: "white" }}>
                  Screen CV
                </div>
              </Grid>
            </Grid>
            <Grid
              item
              container
              md={2}
              sm={4}
              xs={8}
              className={classes.step}
              direction="column"
              justifyContent="flex-start"
              style={{ backgroundColor: cyan[500] }}
            >
              <Grid item container justifyContent="center" alignItems="center">
                <div className={classes.stepImg}>
                  <img src={interview} alt="" />
                </div>
              </Grid>
              <Grid item>
                <div className="c-p text-center" style={{ color: "white" }}>
                  Phone interview
                </div>
              </Grid>
            </Grid>
            <Grid
              item
              container
              md={2}
              sm={4}
              xs={8}
              className={classes.step}
              direction="column"
              justifyContent="flex-start"
              style={{ backgroundColor: red[400] }}
            >
              <Grid item container justifyContent="center" alignItems="center">
                <div className={classes.stepImg}>
                  <img src={test} alt="" />
                </div>
              </Grid>
              <Grid item>
                <div className="c-p text-center" style={{ color: "white" }}>
                  Home test
                </div>
              </Grid>
            </Grid>
            <Grid
              item
              container
              md={2}
              sm={4}
              xs={8}
              className={classes.step}
              direction="column"
              justifyContent="flex-start"
              style={{ backgroundColor: yellow[700] }}
            >
              <Grid item container justifyContent="center" alignItems="center">
                <div className={classes.stepImg}>
                  <img src={meeting} alt="" />
                </div>
              </Grid>
              <Grid item>
                <div className="c-p text-center" style={{ color: "white" }}>
                  Interview
                </div>
              </Grid>
            </Grid>
            <Grid
              item
              container
              md={2}
              sm={4}
              xs={8}
              className={classes.step}
              direction="column"
              justifyContent="flex-start"
              style={{ backgroundColor: purple[900] }}
            >
              <Grid item container justifyContent="center" alignItems="center">
                <div className={classes.stepImg}>
                  <img src={offer} alt="" />
                </div>
              </Grid>
              <Grid item>
                <div className="c-p text-center" style={{ color: "white" }}>
                  Offer
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            container
            justifyContent="center"
            style={{
              transform: !matchesMD ? "translateY(-5em)" : "none",
            }}
          >
            <Button
              variant="contained"
              sx={{
                borderRadius: "20px",
                backgroundColor: red[400],
                padding: "5px 20px",
              }}
              color="red"
            >
              <span className="c--bold" style={{ color: "white" }}>
                Learn more
              </span>
            </Button>
          </Grid>
        </Grid>
      </Grid>

      <Grid
        className="c-cus-container"
        style={{ paddingTop: 0 }}
        direction="column"
        item
        container
      >
        <Grid
          item
          container
          justifyContent="center"
          style={{ marginBottom: "5em" }}
        >
          <span className="c-h3 text-center c--bold">All Open Positions</span>
        </Grid>
        <Grid item container direction="column">
          {jobsList.map((job) => {
            return Job(job);
          })}
        </Grid>
      </Grid>

      <Grid item container className="c-cus-container">
        <Grid
          item
          container
          justifyContent="flex-start"
          style={{ marginBottom: "5em" }}
        >
          <span className="c-h3 text-center c--bold">FAQs</span>
        </Grid>
        <Grid item container rowSpacing={6}>
          {faqs.map((faq, index) => {
            let displayIndex = index + 1;
            return (
              <Grid item container direction="column" md={6}>
                <div className={classes.questionNum}>
                  <span className="c-h3 c--bold" style={{ color: "white" }}>
                    {displayIndex < 10 ? "0" + displayIndex : displayIndex}
                  </span>
                </div>
                <div className="c-p c--bold">{faq.question}</div>
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Jobs;
