import { createTheme } from "@mui/material/styles";
import { grey, orange, red, yellow } from "@mui/material/colors";

const arcBlue = "#0B72B9";
const grey1 = "#091738"
const grey2 = "#17274E"
const grey3 = "#3E5476"
const grey4 = "#E9ECFA"
const grey5 = "#BAC9D7"
const theme = createTheme({
  palette: {
    common: {
      white: "#ffffff",
      orange: "#FFBE16",
      yellow: yellow[600],
      grey: grey[400],
      main: arcBlue,
      darkGrey: "#17274E",
      grey1: grey1,
      grey2: grey2,
      grey3: grey3,
      grey4: grey4,
      grey5: grey5,
      blue: arcBlue
    },

    primary: {
      main: yellow[900],
      light: yellow[600],
    },
    secondary: {
      main: "#FFBE16",
    },
    white: {
      main: "#ffffff",
    },
    grey:{
      main: grey2,
      light: grey[100],
      dark: grey[600]
    },
    blue:{
      main: arcBlue
    },
    black:{
      main: "#000000"
    },
    yellow:{
      main: "#FFBE16",
      light: yellow[500],
    },
    red:{
      main: red[500]

    }
  },

  typography: {
    tab: {
      fontWeight: "700",
      fontFamily: "Gilroy-Regular",
      color: yellow[900],
    },
  },

  components: {
    MuiTab: {
      variants: [
        {
          props: { variant: "header" },
          style: {
            marginLeft: "20px",
            textTransform: "none",
            fontWeight: "700",
            fontFamily: "Gilroy-Regular",
            color: "#FFBE16",
            fontSize: "1rem",
          },
        },
      ],
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          color: "#000000",
          fontSize: "1rem",
        },
      },
    },
    MuiInput: {
      styleOverrides: {
        // underline:{
        //   "&:before":{
        //     borderBottom: `1px solid ${arcBlue}`
        //   },
        //   "&:hover:not($disabled):not($focused):not($error):before":{
        //     borderBottom: `2px solid ${arcBlue}!important`
        //   }
        // }
      },
    },
  },
  overrides: {},
});

export default theme;
