import { ThemeProvider } from "@mui/material/styles";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./assets/scss/App.scss";
import "antd/dist/antd.css";
import "./App.css";
import AboutUs from "./components/AboutUs";
import ContactUs from "./components/ContactUs";
import Footer from "./components/Footer";
import Header from "./components/Header/Header";
import Jobs from "./components/Jobs";
import theme from "./components/Theme";
import Homepage from "./components/home page/Homepage";
import Gallery from "./components/gallery/Gallery";
import { jobs } from "./components/data/jobs";
import Job from "./components/Job";

function App() {
  const jobsList = jobs;
  return (
    <BrowserRouter>
      <div className="App">
        <ThemeProvider theme={theme}>
          <Header />
          <Switch>
            <Route exact path="/" component={() => <Homepage />}></Route>
            <Route exact path="/about" component={() => <AboutUs />}></Route>
            <Route exact path="/life" component={() => <Gallery />}></Route>
            <Route exact path="/jobs" component={() => <Jobs />}></Route>
            {jobsList.map((job, index) => {
              return (
                <Route
                  exact
                  path={"/jobs-details/"+(job.id)}
                  component={() => <Job {...job}/>}
                ></Route>
              );
            })}

            <Route
              exact
              path="/candidates"
              component={() => <div>Refer candidates</div>}
            ></Route>
            <Route
              exact
              path="/contact"
              component={() => <ContactUs />}
            ></Route>
          </Switch>
          <Footer />
        </ThemeProvider>
      </div>
    </BrowserRouter>
  );
}

export default App;
